import re
import copy

from cobra import Model, Reaction, Species, Metabolite, Gene

def add_expanded_reaction(model, reaction_id, formula, proteins, couplers):
    '''Add a reaction as three-stages including the enzyme as a reactant.

    Parameters
    ----------
    model: the Model to add the reaction to
    formula: reaction formula string
    proteins: A list of proteins that catalyze this reaction

    '''
    # Temporary reactions with stars in their IDs are causing trouble
    if reaction_id[0]=='*':
        return

    [species, reaction_is_reversible] = parse_reaction_formula(formula)
    (reactants, products) = split_reaction_sides(species)
        
    for protein in proteins:
        current_reaction_id = '_'.join([reaction_id,protein])

        try:
            enzyme = model.metabolites.get_by_id(protein)
        except KeyError:
            enzyme = Metabolite(protein+'_c',compartment='c')

        expired_enzyme = Metabolite(protein+"_expired_c",
                                    compartment='c')
        diluted_enzyme = Metabolite(protein+"_diluted_c",
                                    compartment='c')

        # It is important to copy since there will be a 3-stage reaction
        # for each protein, so each will have different reactants/products.
        step1_species = reactants.copy()

        step3_species = products.copy()

        es_complex = Metabolite(current_reaction_id+'_es_complex_c',
                                compartment='c')

        ep_complex = Metabolite(current_reaction_id+'_ep_complex_c',
                                compartment='c')

        step1 = Reaction(current_reaction_id+'_A')
        step1_species[es_complex] = 1.0

        step2 = Reaction(current_reaction_id+'_B')
        step2_species = {es_complex: -1.0, ep_complex: 1.0}
        step2.add_metabolites(step2_species)

        step3 = Reaction(current_reaction_id+'_C')
        step3_species[ep_complex] = -1.0

        reaction_list = [step1,step2,step3]
        
        initial_state = {enzyme: -1.0}
        final_state = {enzyme:
                           1.0 - couplers["degradation"] - couplers["dilution"],
                       expired_enzyme:
                           couplers["degradation"],
                       diluted_enzyme:
                           couplers["dilution"]}

        if reaction_is_reversible:
            make_reversible(step2)

            step1_reverse = Reaction(current_reaction_id+"_reverse_A")
            step1_reverse_species = invert_dict(step1_species)
            step1_reverse_species.update(final_state)
            step1_reverse.add_metabolites(step1_reverse_species)

            step3_reverse = Reaction(current_reaction_id+"_reverse_C")
            step3_reverse_species = invert_dict(step3_species)
            step3_reverse_species.update(initial_state)
            step3_reverse.add_metabolites(step3_reverse_species)

            reaction_list += [step1_reverse,step3_reverse]

        step1_species.update(initial_state)
        step1.add_metabolites(step1_species)

        step3_species.update(final_state)
        step3.add_metabolites(step3_species)

        model.add_reactions(reaction_list)

def count_bases(rna_seq,initbases):
    """Count RNA bases in the gene"""
    n_atp = 0
    n_gtp = 0
    n_ctp = 0
    n_utp = 0
    n = 0

    for base in rna_seq:
        base = str.lower(base)
        if base == 'a':
            n_atp += 1
        elif base == 'g':
            n_gtp += 1
        elif base == 'c':
            n_ctp += 1
        elif base == 'u':
            n_utp += 1
        else:
            pass
        n+=1
        if n==initbases:
            n0_atp = copy.copy(n_atp)
            n_atp = 0
            n0_gtp = copy.copy(n_gtp)
            n_gtp = 0
            n0_ctp = copy.copy(n_ctp)
            n_ctp = 0
            n0_utp = copy.copy(n_utp)
            n_utp = 0
        pass

    return [n-initbases,n0_atp,n0_gtp,n0_ctp,n0_utp,n_atp,n_gtp,n_ctp,n_utp]

def parse_reaction_formula(reaction_string):
    '''Adapted from cobra.core.Reaction _parse_reaction function
    
    reaction_string format: atp[c] + h2o[c] --> adp[c] + pi[c]
    '''

    # remove flanking whitespace
    reaction_string = reaction_string.strip()
    #remove parentheses around stoichiometric coefficients
    re_stoich_coeff = re.compile('\(\d+\.{0,1}\d{0,}\)')
    re_spaces = re.compile(' {2,} ')
    the_reaction = re_spaces.subn(' ', reaction_string)[0]
    tmp_reaction = ''
    for the_atom in the_reaction.split(' '):
        tmp_reaction += ' '
        if re_stoich_coeff.match(the_atom) is not None:
            the_atom = the_atom.lstrip('(').rstrip(')')
        tmp_reaction += the_atom
    reaction_string = tmp_reaction[1:]
    if reaction_string[0] == '[' and reaction_string ==']':
        reaction_string = process_prefixed_reaction()
    #Deal with scientific notation here
    tmp_metabolites = re.compile('\d*\.?\d+(e-?\d)? +').sub(
        '', reaction_string)
    tmp_metabolites = re.compile('( *\+ *)|( *<*(-+|=+)> *)').sub(
        '\t', tmp_metabolites).split('\t')
      
    #TODO: Can the user specifying the reversibility override the 
    #      reaction equation?
    #(i.e. If self.reversibility != '' then do the following? )
    #Change the reversible check to a regular expression
    if ' <=> ' in reaction_string or ' <==> ' in reaction_string:
        reversibility = 1
        reaction_delimiter_re = re.compile(' +<=+> +')
    elif ' <-> ' in reaction_string or ' <--> ' in reaction_string:
        reversibility = 1
        reaction_delimiter_re = re.compile(' +<-+> +')
    elif '-> ' in  reaction_string:
        reversibility = 0
        reaction_delimiter_re = re.compile(' +-+> +')
    elif '<- ' in  reaction_string:
        reversibility = 0
        reaction_delimiter_re = re.compile(' +<-+ +')

    [tmp_reactants, tmp_products] = reaction_delimiter_re.split(reaction_string)
    element_re = re.compile(' +\+ +')
    tmp_reactants = element_re.split(tmp_reactants)
    tmp_products = element_re.split(tmp_products)
    tmp_coefficients = []
    for the_reactant in tmp_reactants:
        tmp_split = the_reactant.split(' ')
        if len(tmp_split) > 1:
            tmp_coefficients.append(-1*float(tmp_split[0]))
        else:
            tmp_coefficients.append(-1)
    for the_product in tmp_products:
        tmp_split = the_product.split(' ')
        if len(tmp_split) > 1:
            tmp_coefficients.append(float(tmp_split[0]))
        else:
            tmp_coefficients.append(1)
    metabolites = dict([(Metabolite('%s_%s'%(x[:-3],
                                             x[-2]),
                                    compartment=x[-2]), y)
                        for x, y in zip(tmp_metabolites, tmp_coefficients)])

    return [metabolites, reversibility]

def make_reversible(reaction):
    reaction.lower_bound = -1000.

def split_reaction_sides(reaction_species):
    reactants = dict([(specimen,coefficient) for specimen,coefficient in
                      reaction_species.iteritems() if coefficient < 0])
    products = dict([(specimen,coefficient) for specimen,coefficient in
                     reaction_species.iteritems() if coefficient > 0])

    return (reactants, products)

def invert_dict(metabolites_dict):
    result = metabolites_dict.copy()

    for specimen, stoichiometry in result.iteritems():
        result[specimen] = -stoichiometry

    return result

# Adapted from 
# bytes.com/topic/python/answers/432218-insert-dictionary-into-sql-data-base
def insertDict(connection, tablename, data):
    fields = data.keys()
    values = data.values()
    placeholder = "%s"
    fieldlist = ",".join(fields)
    placeholderlist = ",".join([placeholder] * len(fields))
    query = "insert into %s(%s) values (%s)" % (tablename, fieldlist,
                                                placeholderlist)
    connection.execute(query, values)
