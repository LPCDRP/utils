import sqlite3
from cStringIO import StringIO

def memory_load_database(dbfile):
    global db

    # Load entire database into memory to improve performance
    # -taken from http://stackoverflow.com/a/10856450/2851032
    disk_db = sqlite3.connect(dbfile)
    disk_db.text_factory = str
    tempfile = StringIO()
    for line in disk_db.iterdump():
        tempfile.write('%s\n' % line)
    disk_db.close()
    tempfile.seek(0)

    db = sqlite3.connect(":memory:")
    db.cursor().executescript(tempfile.read())
    db.commit()

    db.text_factory = str

def load_disk_database(dbfile):
    global db

    db = sqlite3.connect(dbfile)
    db.text_factory = str

# Alias for the version to use
load_database = memory_load_database

def get_metabolites():
    query = "SELECT abbreviation, name, formula FROM metabolites"
    return db.execute(query)

def get_count_macromolecules():
    query = "SELECT count(DISTINCT macromolecule) FROM macromolecule_subunits"
    return int(db.execute(query).fetchone()[0])

def get_macromolecules():
    query = ("SELECT DISTINCT macromolecule FROM macromolecule_subunits "
             "ORDER BY macromolecule DESC")
    return db.execute(query).fetchall()

def get_gene(gene_product):
    query = "SELECT locus_tag FROM genes WHERE name=?"
    return db.execute(query,(gene_product,)).fetchone()[0]

def get_gene_product_name(gene):
    query = "SELECT name FROM genes WHERE locus_tag=?"
    return db.execute(query, (gene,)).fetchone()[0]

def get_subunits(macromolecule):
    query = ("SELECT DISTINCT subunit, n_subunit_duplicates "
             "FROM macromolecule_subunits WHERE macromolecule=?")
    return db.execute(query,(macromolecule,)).fetchall()

def get_gene_products(macromolecule,subunit):
    query = ("SELECT gene_product, n_gene_product_duplicates "
             "FROM macromolecule_subunits WHERE macromolecule=? AND subunit=?")
    return db.execute(query,(macromolecule,subunit)).fetchall()

##Queries for load_tRNAs - the schema may change for this
def get_tRNAs():
    query = "SELECT locus_tag, name FROM genes WHERE type='tRNA'"
    return db.execute(query).fetchall()

def get_tRNA_anticodons():
    query = "SELECT gene, anticodon FROM trna"
    return db.execute(query).fetchall()

def get_tRNA_amino_acids():
    query = "SELECT gene, aa FROM trna"
    return db.execute(query).fetchall()

def get_tRNA_transcription_information():
    query = ("SELECT locus_tag, name, tu_id, start_position, stop_position, "
             "strand FROM genes WHERE type='tRNA'")
    return db.execute(query).fetchall()
#########################################################

def get_rRNAs():
    query = ("SELECT locus_tag,name,start_position,stop_position,strand "
             "FROM genes WHERE type='rRNA'")
    return db.execute(query).fetchall()

def get_gene_transcription_units(gene):
    '''
    Get all transcription units containing the gene
    '''
    query = "SELECT tu_id from genes where locus_tag=?"
    return db.execute(query,(gene,)).fetchall()[0]

def get_transcription_units():
    '''
    Based on the macromolecules in the database, return a list of transcription
    units that include their component source genes
    '''
    gene_products = "SELECT DISTINCT gene_product FROM macromolecule_subunits"
    genes = "SELECT DISTINCT locus_tag FROM genes WHERE name IN (%s)" %\
        gene_products

    query = ("SELECT DISTINCT tu.id, tu.start, tu.stop, tu.strand "
             "FROM tu, genes WHERE genes.tu_id=tu.id "
             "AND genes.locus_tag IN (%s)" % genes)
    tu_information = []
    for tu_id, start, stop, strand in db.execute(query):
        tu_type = db.execute("SELECT DISTINCT type FROM genes WHERE tu_id=?",
                             (tu_id,)).fetchall()
        assert(len(tu_type)==1)
        start = int(start)
        stop = int(stop)
        tu_information.append((tu_id,start,stop,strand,tu_type[0][0]))

    return tu_information

def get_tu_component_genes(tu_id):
    query = ("SELECT locus_tag, name, start_position, stop_position "
             "FROM genes WHERE tu_id=?")
    return db.execute(query,(tu_id,)).fetchall()

def get_template_reactions(subject):
    '''subject - What the template is for (examples: tRNA, gene, ...)
    '''
    query = ("SELECT abbreviation_stem, name_stem, formula, subsystem "
             "FROM template_reactions WHERE subject=?")
    return db.execute(query,(subject,)).fetchall()

def get_nontemplate_reactions():
    query = ("SELECT abbreviation_stem, name_stem, formula, subsystem "
             "FROM template_reactions WHERE subject='all'")
    return db.execute(query).fetchall()

def get_metabolic_reactions(subsystems):
    # Get protein list from table enzymes or analogous table once the issue
    # with transporters vs enzymes is sorted out (EC/TC identifier issue)
    query = "SELECT name, formula, protein FROM reactions"
    if subsystems != ["all"]:
        subsystem_list = ', '.join(("'"+each+"'") for each in subsystems)
        query = " %s WHERE subsystem in (%s)" % (query,subsystem_list)

    # The pipe, |, is the delimiter used in data entry
    # [!] Null values appear as empty strings due to SQLite's import function
    result = []
    for name, formula, proteins in db.execute(query):
        protein_list = proteins.split('|')
        result.append((name,formula,protein_list))

    return result

def get_description(gene):
    '''Get the TubercuList short annotation for a given gene'''
    query = 'SELECT description FROM genes WHERE locus_tag=?'

    return db.execute(query,(gene,)).fetchone()[0]

def get_subsystems(gene):
    '''Get the KEGG pathway(s) for a given gene'''
    query = 'SELECT pathway FROM kegg_pathways WHERE gene=?'

    subsystem_list = db.execute(query,(gene,)).fetchall()
    if subsystem_list:
        subsystem_list = [subsystem for _ in subsystem_list for subsystem in _]
        subsystems = ', '.join(subsystem_list)
    else:
        subsystems = ''
    return subsystems

def get_coupling_constants(kind):
    query = "SELECT value FROM parameters WHERE name=?"
    constants = {"dilution":0.0, "degradation":0.0}

    DT = float(db.execute(query,("doubling_time",)).fetchall()[0][0])

    if kind == "enzyme":
        ML = float(db.execute(query,("enzyme_mean_lifetime",)).fetchall()[0][0])
        R = float(db.execute(query,("enzyme_reaction_rate",)).fetchall()[0][0])
    elif kind == "mRNA":
        ML = float(db.execute(query,("mRNA_mean_lifetime",)).fetchall()[0][0])
        R = float(db.execute(query,("translation_rate",)).fetchall()[0][0])
    elif kind == "tRNA":
        ML = 1e5
        R = 1.0
    elif kind == "rRNA":
        ML = 1e5
        R = 1.0

    constants["dilution"] = \
        1/(DT*R)
    constants["degradation"] = \
        1/(ML*R)

    return constants
